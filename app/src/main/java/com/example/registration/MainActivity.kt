package com.example.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val registerButton = findViewById<Button>(R.id.register)

        registerButton.setOnClickListener{

            val firstName = findViewById<TextInputEditText>(R.id.first_name_input_text).text.toString()
            val middleName = findViewById<TextInputEditText>(R.id.middle_name_input_text).text.toString()
            val lastName = findViewById<TextInputEditText>(R.id.last_name_input_text).text.toString()
            val gender = when(findViewById<RadioGroup>(R.id.gender_group).checkedRadioButtonId) {
                R.id.male -> "Male"
                R.id.female -> "Female"
                else -> ""
            }
            val address = findViewById<TextInputEditText>(R.id.address_input_text).text.toString()

            val toast: Toast
            if(!(firstName.isEmpty() || middleName.isEmpty() || lastName.isEmpty() || gender.isEmpty()
                        || address.isEmpty()))
                toast = Toast.makeText(this, "Hello $firstName $lastName!", Toast.LENGTH_LONG)
            else
                toast = Toast.makeText(this, "Please enter all the fields!", Toast.LENGTH_LONG)
            toast.show()
        }
    }
}